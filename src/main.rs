use axum::{
    extract,
    http::StatusCode,
    response::{IntoResponse, Response},
    routing::get,
    Router,
};
use serde::{Deserialize, Serialize};
use std::env::var;
use std::io::prelude::*;
use std::path::Path;

#[tokio::main]
async fn main() {
    let config = get_or_create_config().unwrap();
    let socket_addr = std::net::SocketAddr::new(config.address, config.port);

    let app = Router::new()
        .route("/", get(root))
        .route("/*url", get(tree))
        .with_state(config);

    axum::Server::bind(&socket_addr)
        .serve(app.into_make_service())
        .await
        .unwrap();
}

#[derive(Clone, Serialize, Deserialize)]
enum Handler {
    Ext(String, String),
}

#[derive(Clone, Serialize, Deserialize)]
struct Config {
    address: std::net::IpAddr,
    port: u16,
    handlers: Vec<Handler>,
}

impl Default for Config {
    fn default() -> Self {
        Config {
            address: std::net::IpAddr::V4(std::net::Ipv4Addr::new(0, 0, 0, 0)),
            port: 3001,
            handlers: Vec::new(),
        }
    }
}

fn get_or_create_config() -> std::io::Result<Config> {
    let config_home = var("XDG_CONFIG_HOME").or(var("HOME").map(|v| format!("{}/.config", v)));
    let fss_config_path = config_home
        .map(|c| Path::new(&c).join("fss").join("config.yaml"))
        .map_err(|e| std::io::Error::new(std::io::ErrorKind::Other, e.to_string()));
    fss_config_path.and_then(|path| {
        if !path.exists() {
            path.parent().map(|dir| {
                std::fs::create_dir_all(dir).and(
                    std::fs::File::create(&path)
                        .map(|fd| serde_yaml::to_writer(fd, &Config::default())),
                )
            });
        }
        std::fs::File::open(path).and_then(|fd| {
            serde_yaml::from_reader(fd)
                .map_err(|e| std::io::Error::new(std::io::ErrorKind::Other, e.to_string()))
        })
    })
}

struct Html(String);

impl Html {
    pub fn new() -> Self {
        Html(String::new())
    }

    pub fn tag(tag: &'static str, html: Html) -> Self {
        Html(format!("<{}>{}</{}>", tag, html, tag))
    }

    pub fn link(link: &str, contents: Html) -> Self {
        Html(format!("<a href=\"/{}\">{}</a>", link, contents))
    }
}

impl std::fmt::Display for Html {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> Result<(), std::fmt::Error> {
        self.0.fmt(f)
    }
}

impl std::ops::Add for Html {
    type Output = Self;
    fn add(self, rhs: Html) -> Self {
        Html::from(self.0 + &rhs.0)
    }
}

impl From<String> for Html {
    fn from(s: String) -> Self {
        Html(s)
    }
}

impl From<&str> for Html {
    fn from(s: &str) -> Self {
        Html(s.to_string())
    }
}

impl From<std::io::Error> for Html {
    fn from(e: std::io::Error) -> Self {
        Html::tag("h1", "std::io::Error".into()) + Html::tag("pre", e.to_string().into())
    }
}

impl From<&Path> for Html {
    fn from(path: &Path) -> Self {
        let mut link = ".".to_string();
        path.iter()
            .map(|c| {
                link += &("/".to_owned() + c.to_str().unwrap_or("."));
                Html::from("/") + Html::link(&link, c.to_str().unwrap_or("").into())
            })
            .fold(Html::link(".", "/".into()), std::ops::Add::add)
    }
}

impl From<&mut std::fs::File> for Html {
    fn from(fd: &mut std::fs::File) -> Self {
        let mut buffer = String::new();
        fd.read_to_string(&mut buffer)
            .and(Ok(Html::tag("pre", buffer.into())))
            .into()
    }
}

impl From<std::fs::DirEntry> for Html {
    fn from(entry: std::fs::DirEntry) -> Self {
        Html::link(
            &entry.path().to_str().unwrap_or("#"),
            entry
                .path()
                .file_name()
                .and_then(|f| f.to_str())
                .unwrap_or("<em>error</em>")
                .into(),
        )
    }
}

impl<T, E> From<Result<T, E>> for Html
where
    Html: From<T> + From<E>,
{
    fn from(r: Result<T, E>) -> Self {
        r.map(Html::from).unwrap_or_else(Html::from)
    }
}

impl<A> FromIterator<A> for Html
where
    Html: From<A>,
{
    fn from_iter<I: IntoIterator<Item = A>>(iter: I) -> Self {
        Html::tag(
            "ul",
            iter.into_iter()
                .map(|e| Html::tag("li", Html::from(e)))
                .fold(Html::new(), std::ops::Add::add),
        )
    }
}

impl IntoResponse for Html {
    fn into_response(self) -> Response {
        axum::response::Html(self.0).into_response()
    }
}

fn render_path(path: &Path) -> Response {
    if path.is_dir() {
        let header = Html::tag("h1", path.into());
        let entries = std::fs::read_dir(path).map(|d| d.filter_map(Result::ok).collect::<Html>());
        (header + Html::from(entries)).into_response()
    } else {
        let download_binary = |_| {
            std::fs::read(path)
                .map(IntoResponse::into_response)
                .unwrap_or(StatusCode::INTERNAL_SERVER_ERROR.into_response())
        };
        std::fs::File::open(path)
            .and_then(|mut fd| {
                let mut buffer = String::new();
                fd.read_to_string(&mut buffer)
                    .and(Ok(Html::tag("pre", buffer.into()).into_response()))
            })
            .unwrap_or_else(download_binary)
    }
}

async fn tree(extract::Path(url): extract::Path<String>) -> Response {
    render_path(Path::new(&url)).into_response()
}

async fn root() -> Response {
    render_path(Path::new(".")).into_response()
}
